default['memcached-gt']['pkg_to_remove']       = [ "nfs-utils", "nfs-utils-lib" ]
default['memcached-gt']['pkg_to_install']      = [ "libevent", "memcached" ]
default['memcached-gt']['memcached_repo']      = '/etc/yum.repos.d/memcached.repo'
default['memcached-gt']['memcached_limits']    = "/etc/security/limits.d/89-mcd.override.conf"


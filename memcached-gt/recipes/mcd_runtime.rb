#
# Cookbook Name:: memcached-gt
# Recipe:: mcd_runtime
#
# Author:: Prashant Joseph
#
# Copyright 2014, GT Nexus, Inc.
#
# All rights reserved - Do Not Redistribute
#memcache runtime service.




cookbook_file node['memcached-gt']['memcached_repo'] do
  mode 00644
end

#override memcached limits.
cookbook_file node['memcached-gt']['memcached_limits'] do
  owner "root"
  group "root"
  mode  00644
end

#
# Cookbook Name:: memcached-gt
# Recipe:: default
#
# Author:: Prashant Joseph
#
# Copyright 2014, GT Nexus, Inc.
#
# All rights reserved - Do Not Redistribute
#install and setup memcache service.

group "memcached" do
  action :create
  gid 1000
end

user "memcached" do
 action :create
 comment "memcached service account"
 gid "memcached"
 uid 1000
 shell "/sbin/nologin"
 supports :manage_home=>false
end

service "memcached" do
  action [ :nothing ]
  supports :status => true, :start => true, :stop => true, :restart => true
end

node['memcached-gt']['pkg_to_remove'].each do |pkg|
  package pkg do
  	action :remove 
  end
end


#cookbook_file node['memcached-gt']['memcached_repo'] do
#  mode 00644
#end

yum_package "libevent" do
#  version "1.4.9-1"
#  allow_downgrade true
#  flush_cache [:before]
end

yum_package "memcached" do
#  version "1.2.6-1"
#  allow_downgrade true
end

mything = data_bag_item("myapp", "mysql")
stackthing = data_bag_item("stackapp", "mysql")
#app = ENV['APP_PASSWORD']
#app = node[:deploy][:app_key_file][:environment_variables][:APP_PASSWORD]
apps = search(:aws_opsworks_app).first
app = apps['environment']['APP_PASSWORD']
#Chef::Log.info("The username is '#{mything['username']}' ")
uname = mything['username']
password = stackthing['password']

template "/etc/init.d/memcached" do
  owner "root"
  group "root"
  mode '0755'
  variables({
  :uname => uname,
  :password => password,
  :app => app
  })
  notifies :restart, resources(:service => "memcached")
end


service "memcached" do 
        action [ :start, :enable ] 
end

name             'zookeeper-gt'
maintainer       'YOUR_COMPANY_NAME'
maintainer_email 'YOUR_EMAIL'
license          'All rights reserved'
description      'Installs/Configures zookeeper-gt'
long_description IO.read(File.join(File.dirname(__FILE__), 'README.md'))
version          '0.1.0'
depends "lvm", ">= 1.0.0"
depends "java", ">= 1.16.0"

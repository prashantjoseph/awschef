#
# Cookbook Name:: zookeeper-gt
# Recipe:: java_install
#
# Author:: Prashant Joseph
#
# Copyright 2014, GT Nexus, Inc.
#
# All rights reserved - Do Not Redistribute
#installs java
#https://thegrid.gtnexus.local/prodops/Install%20Sun%20JRE%20on%20RHEL%20and%20CentOS


directory node['zookeeper-gt']['java_dir'] do
  owner "root"
  group "root"
  mode 0755
end

remote_file node['zookeeper-gt']['java_pkg'] do
  source node['zookeeper-gt']['java_download']
  owner 'root'
  group 'root'
  mode 00755
end

execute "install_java" do
    cwd "/opt/java"
    command "echo 'yes' | #{node['zookeeper-gt']['java_pkg']} &>/dev/null"
    not_if { ::File.exists?("#{node['zookeeper-gt']['java_cmd']}") }
end

cookbook_file node['zookeeper-gt']['java_env'] do
  mode 00755
end

execute "update-alternatives --install /usr/bin/java java #{node['zookeeper-gt']['java_cmd']} 1 && update-alternatives --set java #{node['zookeeper-gt']['java_cmd']}" do
  not_if "update-alternatives --display java | grep '#{node['zookeeper-gt']['java_cmd']} - priority 1'"
end

execute "setup_envs" do
  command "./etc/profile.d/java.sh"
end

#
# Cookbook Name:: zookeeper-gt
# Recipe:: default
#
# Author:: Prashant Joseph
#
# Copyright 2014, GT Nexus, Inc.
#
# All rights reserved - Do Not Redistribute
#
#Zookeeper server install 

#include_recipe "lvm::zk_lvm"
mnt = Array.new
File.open '/proc/mounts' do |io|
        mnt = io.grep(/\/database\/zookeeper\/txlogs/ )
   end
if ( mnt.empty?)
    include_recipe "lvm::zk_lvm"
else
  
  Chef::Log.fatal("db2deploy: DEBUG /database/zookeeper/txlogs is mounted, Recipe run will be skipped.")
end


#if node.has_key? "block_device"
#if node["block_device"].has_key? "sde"
#if node["block_device"].has_key? "sdf"
#  include_recipe "lvm::zk_lvm"
#end
#end
#end

if node.has_key? "filesystem"     
  if node["filesystem"].has_key? "/dev/mapper/vg_database-lv_database"        
    if node["filesystem"]["/dev/mapper/vg_database-lv_database"].has_key? "mount"           
      if node['filesystem']['/dev/mapper/vg_database-lv_database']['mount'] == '/database/zookeeper/data'
        if node["filesystem"].has_key? "/dev/mapper/vg_logs-lv_logs"        
          if node["filesystem"]["/dev/mapper/vg_logs-lv_logs"].has_key? "mount"           
            if node['filesystem']['/dev/mapper/vg_logs-lv_logs']['mount'] == '/database/zookeeper/txlogs'   
              include_recipe "zookeeper-gt::java_install"

              group node['zookeeper-gt']['group'] do
                action :create
                gid 20103
              end

              user node['zookeeper-gt']['user'] do
               action :create
               comment "zookeeper service account"
               gid "zookeeper"
               uid 20103
               home "/opt/zookeeper"
              end

              node['zookeeper-gt']['conf_dirs'].each do |dirs|
               directory dirs do
              #directory node['zookeeper-gt']['conf_dir'] do
                owner node['zookeeper-gt']['user']
                group node['zookeeper-gt']['group']
                mode 0755
                action :create
                recursive true
              end
              end

              directory node['zookeeper-gt']['log_dir'] do
                owner node['zookeeper-gt']['user']
                group node['zookeeper-gt']['group']
                mode 0755
                action :create
                recursive true
              end

              #Disabled: gitsync recipe will take care of droping scripts
              #directory node['zookeeper-gt']['tc_bin'] do
              #  owner node['zookeeper-gt']['user']
              #  group node['zookeeper-gt']['group']
              #  mode 0755
              #  action :create
              #  recursive true
              #end
              #
              #node['zookeeper-gt']['tc_bin_files'].each do |files|
              #  cookbook_file files do
              #    action :create
              #    mode 00777
              #   end
              #end

              cookbook_file node['zookeeper-gt']['cron_job'] do
                action :create
              end

              cookbook_file node['zookeeper-gt']['zookeeper_repo'] do
                mode 00644
              end


              #%w{ "bigtop-utils", "netcat.x86_64 = 1.10-891.1", "zookeeper.noarch = 3.4.3+27-1.cdh4.1.1.p0.7.el6",  "zookeeper-server.noarch = 3.4.3+27-1.cdh4.1.1.p0.7.el6" }.each do |pck|
              #%w{ "bigtop-utils.noarch = 0.4+354-1.cdh4.1.1.p0.7.el6", "netcat.x86_64 = 1.10-891.1", "zookeeper.noarch = 3.4.3+27-1.cdh4.1.1.p0.7.el6",  "zookeeper-server.noarch = 3.4.3+27-1.cdh4.1.1.p0.7.el6" }.each do |pck|

              yum_package "bigtop-utils" do
                version "0.4+354-1.cdh4.1.1.p0.7.el6"
                allow_downgrade true
                flush_cache [:before]
              end
              #end

              yum_package "netcat" do
                version "1.10-891.1"
                allow_downgrade true
                flush_cache [:before]
              end


              yum_package "zookeeper" do
                version "3.4.3+27-1.cdh4.1.1.p0.7.el6"
                allow_downgrade true
                flush_cache [:before]
              end


              yum_package "zookeeper-server" do
                version "3.4.3+27-1.cdh4.1.1.p0.7.el6"
                allow_downgrade true
                flush_cache [:before]
              end

              node['zookeeper-gt']['conf_files'].each do |files|
              cookbook_file files do
                owner node['zookeeper-gt']['user']
                group node['zookeeper-gt']['group']
                mode 00644
              end
              end

              #remote_file "#{node['zookeeper-gt']['zoo_conf_dir']}/zoo.cfg" do
              #  source "#{node['zookeeper-gt']['conf_download_dir']}/#{node[:server_env]}/zk/zoo.cfg"
              #  notifies :reload, "service[zookeeper-server]", :delayed
              #end

              #cookbook_file "#{node['zookeeper-gt']['zoo_conf_dir']}/zoo.cfg" do
              #  source "conf/#{node[:server_env]}/zoo.cfg"
              #  owner node['zookeeper-gt']['user']
              #  group node['zookeeper-gt']['group']
              #  mode 00644
              #end

              #generate zoo.conf using data_bags
              #Added new logic to include multiple clusters
              #zk = data_bag_item("zookeeper", "#{node[:server_env]}")
              #zk_nodes = zk['zookeeper']

              zk_cluster = node[:tc_services]
              zk = data_bag_item("zookeeper", "#{node[:server_env]}")
              zk_nodes = zk["#{zk_cluster}"]
		#Chef::Log.debug("zk_cluster is #{zk_cluster}")
		#Chef::Log.debug("zk_nodes is #{zk_nodes}")


              template "/etc/zookeeper/conf/zoo.cfg" do
                source "zoo.cfg.erb"
                owner "root"
                group "root"
                mode "0644"
               variables :zookeeper => zk_nodes
		notifies :reload, "service[zookeeper-server]", :delayed
              end

              template "/etc/zookeeper/conf/java.env" do
                owner node['zookeeper-gt']['user']
                group node['zookeeper-gt']['group']
                mode '00644'
		notifies :reload, "service[zookeeper-server]", :delayed
              end


              execute "/etc/init.d/zookeeper-server init && echo true > /var/local/.initialize_zookeeper_server" do
                not_if do
                  File.exists?("/var/local/.initialize_zookeeper_server")
                end
              end

              template "/database/zookeeper/data/myid" do
                owner node['zookeeper-gt']['user']
                group node['zookeeper-gt']['group']
                mode '00644'
              end

              service "zookeeper-server" do
                      action [ :start, :enable ]
              end

            
            end
          end
        end
      end
    end
  end
end


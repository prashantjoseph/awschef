#
# Cookbook Name:: zookeeper-gt
# Recipe:: zk_runtime
#
# Author:: Prashant Joseph
#
# Copyright 2014, GT Nexus, Inc.
#
# All rights reserved - Do Not Redistribute
#
#Zookeeper server runtime recipe


if node.has_key? "filesystem"     
  if node["filesystem"].has_key? "/dev/mapper/vg_database-lv_database"        
    if node["filesystem"]["/dev/mapper/vg_database-lv_database"].has_key? "mount"           
      if node['filesystem']['/dev/mapper/vg_database-lv_database']['mount'] == '/database/zookeeper/data'
        if node["filesystem"].has_key? "/dev/mapper/vg_logs-lv_logs"        
          if node["filesystem"]["/dev/mapper/vg_logs-lv_logs"].has_key? "mount"           
            if node['filesystem']['/dev/mapper/vg_logs-lv_logs']['mount'] == '/database/zookeeper/txlogs'   



              node['zookeeper-gt']['conf_dirs'].each do |dirs|
               directory dirs do
              #directory node['zookeeper-gt']['conf_dir'] do
                owner node['zookeeper-gt']['user']
                group node['zookeeper-gt']['group']
                mode 0755
                action :create
                recursive true
              end
              end

              directory node['zookeeper-gt']['log_dir'] do
                owner node['zookeeper-gt']['user']
                group node['zookeeper-gt']['group']
                mode 0755
                action :create
                recursive true
              end




              cookbook_file node['zookeeper-gt']['zookeeper_repo'] do
                mode 00644
              end

                      
            end
          end
        end
      end
    end
  end
end


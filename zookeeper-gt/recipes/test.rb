#test using databag

#data_bag("zookeeper")
#zk = data_bag_item("zookeeper", "test")
zk = data_bag_item("zookeeper", "#{node[:server_env]}")
zk_nodes = zk['zookeeper']

%w{/etc/zookeeper /etc/zookeeper/conf}.each do |d|
directory d do
	owner "root"
	group "root"
	mode   00775
	end
end



template "/etc/zookeeper/conf/zoo.cfg" do
  #path "/etc/zookeeper/conf/"
  source "zoo.cfg.erb"
  owner "root"
  group "root"
  mode "0644"
 variables :zookeeper => zk_nodes
end

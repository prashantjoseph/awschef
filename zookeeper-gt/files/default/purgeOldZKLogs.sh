#!/bin/sh

NUM_BACKUPS=${1:-12}

ZK_BASE=${2:-"/usr/lib/zookeeper"}

ZK_CLASSPATH="${ZK_BASE}/zookeeper.jar:${ZK_BASE}/lib/log4j-1.2.15.jar:conf:${ZK_BASE}/lib/jline-0.9.94.jar:${ZK_BASE}/lib/slf4j-api-1.6.1.jar:${ZK_BASE}/lib/slf4j-log4j12-1.6.1.jar"

ZK_DATA_HOME="/database/zookeeper"

ZK_DATA_DIRS="${ZK_DATA_HOME}/txlogs ${ZK_DATA_HOME}/data"

/usr/bin/java -cp ${ZK_CLASSPATH} org.apache.zookeeper.server.PurgeTxnLog ${ZK_DATA_DIRS} -n ${NUM_BACKUPS}

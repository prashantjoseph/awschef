#!/bin/sh

FILENAME=${HOSTNAME}.`date +%Y%m%d%H%M`.dump.out
echo $FILENAME
echo dump | nc localhost $(grep "^[[:space:]]*clientPort" /etc/zookeeper/conf/zoo.cfg | sed -e 's/.*=//') > $FILENAME

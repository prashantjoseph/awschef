#!/bin/sh

query_hosts=`grep "^server" /etc/zookeeper/conf/zoo.cfg | sed -e 's/^server.[0-9]*=\([a-zA-Z0-9]*-zk-[0-9]*-n[0-9]*\):.*$/\1/'`
for f in $query_hosts;
do
 echo "$f -> ";
 echo srvr | nc ${f} 2181;
done

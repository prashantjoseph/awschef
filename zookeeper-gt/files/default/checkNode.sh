#!/bin/sh

echo stat | nc localhost $(grep "^[[:space:]]*clientPort" /etc/zookeeper/conf/zoo.cfg | sed -e 's/.*=//')
